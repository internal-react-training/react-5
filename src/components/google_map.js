import React, { Component } from 'react';

class GoogleMap extends Component {
	componentDidMount() {
		// this.refs.map <- is where the map will render to
		// second argument is options
		new google.maps.Map(this.refs.map, {
			zoom: 12,
			center: {
				lat: this.props.lat,
				lng: this.props.lon
			}
		});
	}
	render() {
		// this.refs.map
		return <div ref="map" />;
	}
}

export default GoogleMap;
