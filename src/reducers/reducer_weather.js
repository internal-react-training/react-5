import { FETCH_WEATHER } from '../actions/index';

// first argument is state, second argument is action
export default function(state = [], action) {
	switch (action.type) {
		case FETCH_WEATHER:
			// this one is bad na. we dont manipulate state directly. we dont mutate our state
			// return state.push(action.payload.data)

			// this one is good. we return a new instance of our state
			// return state.concat([action.payload.data]);

			// this one is better. make a new array, take action.payload.data inside of it,
			// then take 'state' and insert it in the array(that's what the '...' means)
			// result = [ city, city, city ] NOT [ city, [ city, city ] ]
			// ps: the action.payload.data is at the top of the array pag ganito
			return [action.payload.data, ...state];
	}
	return state;
}
