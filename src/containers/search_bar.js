import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchWeather } from '../actions/index';

export class SearchBar extends Component {
	constructor(props) {
		super(props);
		this.state = { term: '' };

		// this line says that 'this'(onInputChange) is being binded into 'this' (SearchBar)
		// and then lagay yung value to this.onInputChange.
		// So everytime this.onInputChange is called, yung binded version na yung tinatawag
		// in short, take a function, bind it to 'this' and replace it
		this.onInputChange = this.onInputChange.bind(this);
		this.onFormSubmit = this.onFormSubmit.bind(this);
	}
	// all DOM event handlers come with event object
	onInputChange(event) {
		// console.log(event.target.value);
		this.setState({ term: event.target.value });
	}

	onFormSubmit(event) {
		event.preventDefault();

		// fetch weather data
		this.props.fetchWeather(this.state.term);
		this.setState({ term: '' });
	}

	render() {
		return (
			<form onSubmit={this.onFormSubmit} className="input-group">
				<input
					placeholder="Get a five-day forecast in your favorite cities"
					className="form-control"
					value={this.state.term}
					onChange={this.onInputChange} //this is not a fat arrow function so we have to bind
				/>
				<span className="input-group-btn">
					<button type="submit" className="btn btn-secondary">
						Submit
					</button>
				</span>
			</form>
		);
	}
}

//hookup fetchWeather to SearchBar
function mapDispatchToProps(dispatch) {
	return bindActionCreators({ fetchWeather }, dispatch);
}

//null because it goes as the 2nd argument in mapDispatchtoProps
export default connect(
	null,
	mapDispatchToProps
)(SearchBar);
