import axios from 'axios';

const API_KEY = 'a48a31de9c3df59feeb18b6bb42af36a';
// es6 syntax
const ROOT_URL = `https://api.openweathermap.org/data/2.5/forecast?appid=${API_KEY}`;

export const FETCH_WEATHER = 'FETCH_WEATHER';

export function fetchWeather(city) {
	const url = `${ROOT_URL}&q=${city},us`;
	// make a get request on the url
	const request = axios.get(url);

	return {
		// as a convention, this is not a string to keep consistency between the 'type' in action and the 'type' in reducers
		type: FETCH_WEATHER,
		// the payload is an optional property
		payload: request
	};
}
